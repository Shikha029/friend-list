import React, { Component } from 'react';
import './index.css';
import { Row, Col } from 'react-bootstrap';
import { FaStar, FaTrash } from 'react-icons/fa';

class FriendRow extends Component {
	constructor(props) {
        super(props);
    }

    render() {
        return (
            <Row>
                <Col xs={8}>
                    <div className="friend-name">{this.props.friend.friendName}</div>
                    <div className="friend-subtitle">{this.props.friend.gender}</div>
                </Col>
                <Col>
                    <div className="icons">
                        <FaStar className={"star-icon " + (!this.props.friend.isFavorite ? "not-favorite" : "")} onClick={() => this.props.performAction(this.props.friend, 'favorite')} /> <FaTrash className="star-icon" onClick={() => this.props.performAction(this.props.friend, 'delete')} />
                    </div>
                </Col>
            </Row>
        )
    }
}

export default FriendRow;