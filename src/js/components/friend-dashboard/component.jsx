import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import FriendRow from '../friend-row/component.jsx';
import { Collapse } from 'reactstrap';
import './index.css';
import firebase from '../../../firebase.js';

class FriendDashboard extends Component {
	constructor() {
        super();

        this.state = {
            friendData: [],
            isGenderVisible: false,
            friendAdded: {},
            friendName: '',
            firebaseReference: {}
        }

        this.handleInputEnter = this.handleInputEnter.bind(this);
        this.setFriendGender = this.setFriendGender.bind(this);
        this.performAction = this.performAction.bind(this);
        this.updateFirebaseObject = this.updateFirebaseObject.bind(this);
    }

    componentDidMount() {
        const fRef = firebase.database().ref('friends');
        fRef.on('child_added', (snapshot) => {
            this.setState({
                firebaseReference: fRef,
                friendData: [...this.state.friendData, snapshot.val()]
            });
        });

        fRef.on('child_changed', (snapshot) => {
            this.updateFirebaseObject('update', snapshot.val());
        })

        fRef.on('child_removed', (snapshot) => {
            this.updateFirebaseObject('delete', snapshot.val());
        })
    }

    updateFirebaseObject(task, field) {
        let friend = this.state.friendData;
        
        friend.forEach((value, key) => {
            if (field.friendName === value.friendName) {
                if (task === 'update') {
                    friend[key] = field;
                    this.state.firebaseReference.child(key).update(field);
                } else {
                    friend.splice(key, 1);
                    this.state.firebaseReference.child(key).remove();
                }
                this.setState({
                    friendData: friend
                });
            }
        });
    }

    performAction(friend, task) {
        let index = this.state.friendData.indexOf(friend);

        if (index !== -1) {
            if (task == 'delete') {
                this.updateFirebaseObject('delete', friend);
            } else {
                friend.isFavorite = !friend.isFavorite;
                this.updateFirebaseObject('update', friend);
            }
        }
    }

    handleInputEnter(event) {
        if (event.key === 'Enter') {
            if (event.target.value) {
                let value = {
                    friendName: event.target.value,
                    gender: '',
                    isFavorite: false
                };
                this.setState({
                    isGenderVisible: true,
                    friendAdded: value
                });
            }
        }
    }

    setFriendGender(gender) {
        let addedFriend = this.state.friendAdded;
        addedFriend.gender = gender;
        this.setState({
            friendData: [...this.state.friendData, addedFriend],
            isGenderVisible: false
        });
    }

    render() {
        return (
            <div>
                <div className="header">Friends List</div>
                <input className="input-friend" type="text" defaultValue={this.state.friendName} placeholder="Type the name of a friend" onKeyDown={this.handleInputEnter}/>
                <Collapse isOpen={this.state.isGenderVisible}>
                    <div className="gender-collapse">
                        <label>Select Gender:</label>
                    </div>
                    <div className="gender-collapse">
                        <input type="radio" name="female" onClick={() => this.setFriendGender('Female')} />Female
                        <input type="radio" name="male" onClick={() => this.setFriendGender('Male')} />Male
                    </div>
                </Collapse>
                {
                    this.state.friendData.map((friend, index) => {
                        return <FriendRow key={index} friend={friend} performAction={this.performAction} />
                    })
                }
            </div>
        )
    }
}

export default FriendDashboard;

const wrapper = document.getElementById("friend-list");
wrapper ? ReactDOM.render(<FriendDashboard />, wrapper) : false;